// Name: Lab_4.8.c
// Time: 16:02 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to print a bigger string first

#include<stdio.h>
#include<string.h>

int main()
{
	char a[20], a1[20];
	scanf("%s", a);
	scanf("%s", a1);
	if(strcmp(a,a1)>0)
	{
		printf("%s%s\n",a,a1);
	}
	else
	{
		printf("%s%s",a1,a);
	}
	
	return 0;
}
