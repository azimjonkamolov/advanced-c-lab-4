// Name: Lab_4.2.c
// Time: 12:19 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to print int as a reverse order string

#include<stdio.h>
#include<string.h>

int main()
{
	char word[20];
	int i, num, sto=0;
	scanf("%d", &num);
	for(i=0;num!=0;i++)
	{
		sto=sto*10;
		sto+=num%10;
		num=num/10;
	}
	
    sprintf(word, "%d", sto); 
	printf("%s", word);
	
	//printf("%s", word);
	
	return 0;
	
}
