// Name: Lab_4.6.c
// Time: 16:02 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to insert string into another

#include <stdio.h>
#include <string.h>

int main()
{
	char st[20], st1[20], st2[50], st3[40];
	int x;
	scanf("%s", st);
	scanf("%s", st1);
	scanf("%d", &x);
	if(x==0)
	{
		strcat(st,st1);
		printf("%s", st);
	}
	else if(x!=0)
	{
		strncpy(st2,st,x);
		strcat(st2,st1);
		sprintf(st3, "%*s", strlen(st)-x, st+x);
		strcat(st2,st3);
		printf("%s", st2);
	}

	return	0;
}