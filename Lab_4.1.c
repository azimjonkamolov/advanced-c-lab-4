// Name: Lab_4.1.c
// Time: 12:13 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to print only lower case letters

#include<stdio.h>
#include<string.h>

int main()
{
	char word[25];
	int i;
	scanf("%s", word);
	for(i=0;word[i]!='\0';i++)
	{
		if(word[i]>='a' && word[i]<='z')
			printf("%c",word[i]);
		else
			continue;
	}
	
	return 0;
}
