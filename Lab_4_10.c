// Name: Lab_4.10.c
// Time: 20:19 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to get five strings and print out them as alphabetic order

#include<stdio.h>
#include <string.h>

int main()
{
    int i, j;
    char ar[100][100], temp[100];
	
    for(i=0; i<5; ++i)
        scanf("%s[^\n]", ar[i]);

    for(i=0; i<5; ++i)
    {
        for(j=i+1; j<5 ; ++j)
        {
            if(strcmp(ar[i], ar[j])>0)
            {
                strcpy(temp, ar[i]);
                strcpy(ar[i], ar[j]);
                strcpy(ar[j], temp);
            }
        }
	}
    for(i=0; i<5; ++i)
    {
        printf("%s\n", ar[i]);
    }

    return 0;
}
