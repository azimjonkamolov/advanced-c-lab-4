// Name: Lab_4.5.c
// Time: 14:02 11.15.2018 (KST)
// Author: Azimjon Kamolov
// Contact: azimjon.6561@gmail.com
// Purpose: to get n number of string and print out the shortest one

#include<stdio.h>
#include<string.h>

int main()
{
	char a[100], a1[100];
	int i, l=100, m, n;
	
	scanf("%d", &n);
	for(i=1;i<=n;i++)
	{
		scanf("%s", a);
		m=strlen(a);
		if(l>m)
		{
			l=m;
			strcpy(a1, a);
		}
		else if( l == m)
		{
			strcpy(a1,a);
		}
	}
	
	printf("%s", a1);
	return 0;
}
